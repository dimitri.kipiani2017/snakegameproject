console.log(`Page Loaded!!!`);

const board = document.getElementById("board");
let boardScale = 10;

let point = function Point(xCoord,yCoord){
    this.x = xCoord;
    this.y =yCoord;
};

let snake = [new point(0,0),new point(1,0),new point(2,0)];
let snakeHead = snake[snake.length-1];
let gameOver = false;

let points = 0;
let highScore = 0;
let speed = 400;

const successSound = document.getElementById('audio');
const failSound = document.getElementById('fail-sound');
const superPointSound = document.getElementById('super');


let direction = 'right';
const cellArray = document.getElementsByClassName('cell');
const showCurrentPoint = document.getElementById('point-span');
const showMaxScore = document.getElementById('score-span');
const  gameOverElement = document.getElementById('game-over');
const restartButton = document.getElementById('btn');

let interval = setInterval(snakeMove,speed);


init();


function init() {
	showCurrentPoint.innerHTML = points;
    showMaxScore.innerHTML = highScore;
    addKeyListeners();
    generateMatrix(boardScale);        
    drawSnake();
    summonPointCell();
    interval;
}




function generateMatrix(size){
    for(let y = 0; y < size; y++){
        for(let x = 0; x < size; x++){
            let cell = document.createElement('div');

            cell.classList.add('cell')
            cell.dataset.x = x.toString();
            cell.dataset.y = (-y).toString();

            board.appendChild(cell);
        }
    }
}

function drawSnake() {
    for (let i = 0; i < cellArray.length; i++){
        cellArray[i].classList.remove("snake-cell");
        cellArray[i].classList.remove("snake-head");
        for (let j=0; j < snake.length; j++){
            if(cellArray[i].dataset.x == snake[j].x && cellArray[i].dataset.y == snake[j].y ){
                cellArray[i].classList.add("snake-cell");
            }
            if(cellArray[i].dataset.x == snakeHead.x && cellArray[i].dataset.y == snakeHead.y ){
                cellArray[i].classList.add('snake-head');
            }
        }
    }
}

function snakeMove() {
    if(!gameOver){
        shiftPush();
        drawSnake();
        if(document.getElementsByClassName("super-point-cell").length < 1){
            summonSuperPointCell();
        }
        discardSuperPoint();
    }
    else{
        clearInterval(interval);
    }
}


function shiftPush() {
    let newHead;
    let pointCellElement = document.getElementsByClassName("point-cell")[0];
    let superPointCellElement = document.getElementsByClassName("super-point-cell")[0];

    switch (direction) {
        case  'left':
            newHead = (snakeHead.x !=0)? new point(snakeHead.x-1,snakeHead.y) : new point(9, snakeHead.y);
            break;
        case 'up':
            newHead = (snakeHead.y !=0)? new point(snakeHead.x,snakeHead.y+1) : new point(snakeHead.x,-9);
            break;
        case 'right':
            newHead = (snakeHead.x !=boardScale-1)? new point(snakeHead.x+1,snakeHead.y) : new point(0, snakeHead.y);
            break;
        case 'down':
            newHead = (snakeHead.y !=-boardScale+1)?new point(snakeHead.x,snakeHead.y-1) : new point(snakeHead.x, 0);
            break;
        default :
            console.log('Something went wrong during the game');
    }



    if(checkIfContained(newHead.x, newHead.y)){
        failToDos();
    }

    if(checkSuccess(pointCellElement.dataset.x, pointCellElement.dataset.y)){
            snakeGotPoint();
            pointCellElement.classList.remove("point-cell");
            pointCellElement.style = '';
            if(!gameOver) snake.push(newHead);
            summonPointCell();
    }
    else{
        if(!gameOver){
            snake.shift();
            snake.push(newHead);
        }
    }


    if(superPointCellElement){
        if(checkSuccess(superPointCellElement.dataset.x, superPointCellElement.dataset.y)){
            snakeGotSuperPoint();
            superPointCellElement.classList.remove("super-point-cell");
        }
    }



    snakeHead = snake[snake.length-1];
}

function snakeGotSuperPoint(){
    points+=10;
    if(points > highScore){
        highScore = points;
    }
    superPointSound.pause();
    superPointSound.play();
    showCurrentPoint.innerHTML = points;
}


function restartGame() {
    gameOverElement.style.display = 'none';
    snake = [new point(0,0),new point(1,0),new point(2,0)];
    snakeHead = snake[snake.length - 1];
    drawSnake();
    gameOver = false;
    direction = 'right';
    points = 0;
    speed = 350;
    showCurrentPoint.innerHTML = points;
    showMaxScore.innerHTML = highScore;
    interval = setInterval(snakeMove,speed);
    interval;
}

function summonPointCell() {
    let pointCell = new point(randomGenerator(),-randomGenerator());

    if(checkIfContained(pointCell.x, pointCell.y)){
        summonPointCell();
    }
    else{
        drawNewPointCell(pointCell.x, pointCell.y);
    }
}

function summonSuperPointCell() {
    if(points !== 0 && points % 10 == 0){
        let superPointCell = new point(randomGenerator(),-randomGenerator());

        if(checkIfContained(superPointCell.x, superPointCell.y)){
            summonSuperPointCell();
        }
        else{
            drawNewSuperPointCell(superPointCell.x, superPointCell.y);
            points++;
        }
    }
}

function discardSuperPoint() {
    let superPointCellElement = document.getElementsByClassName("super-point-cell")[0];
    if(superPointCellElement){
        if(speed > 350)
        setTimeout(function () {
            superPointCellElement.classList.remove("super-point-cell");
        }, 5000);
        else if(speed < 350){
            setTimeout(function () {
                superPointCellElement.classList.remove("super-point-cell");
            }, 3000);
        }
        else if(speed < 300){
            setTimeout(function () {
                superPointCellElement.classList.remove("super-point-cell");
            }, 2000);
        }
    }
}

function drawNewSuperPointCell(xCoord, yCoord){
    for(let i = 0; i < cellArray.length; i++){
        if(cellArray[i].dataset.x == xCoord && cellArray[i].dataset.y == yCoord){
            cellArray[i].classList.add("super-point-cell");
        }
    }
}


function checkSuccess(xCoord, yCoord){
    if(snakeHead.x == Number(xCoord) && snakeHead.y == Number(yCoord)){
        return true;
    }
    else{
        return false;
    }
}


function checkIfContained(xCoord, yCoord) {
    let snakeContainsPointCell = false;

    for(let i = 0; i < snake.length; i++){
        if(snake[i].x == xCoord && snake[i].y == yCoord){
            snakeContainsPointCell = true;
        }
    }
    return snakeContainsPointCell;
}


function drawNewPointCell(xCoord, yCoord) {
    for(let i = 0; i < cellArray.length; i++){
        if(cellArray[i].dataset.x == xCoord && cellArray[i].dataset.y == yCoord){
            cellArray[i].classList.add("point-cell");
            cellArray[i].style.backgroundColor = randomRGB();
        }
    }
}

function failToDos() {
    failSound.play();
    gameOver = true;
    gameOverElement.style.display = 'block';
    clearInterval(interval);
    showMaxScore.innerHTML = highScore;
}

function snakeGotPoint() {
    points++;
    if(points > highScore){
        highScore = points;
    }
    successSound.pause();
    successSound.play();
    showCurrentPoint.innerHTML = points;
    speed-=5;
}

function addKeyListeners() {
    document.addEventListener("keydown", function (e) {
        if(e.keyCode === 37 && direction !=='right' && direction !=='left' && !gameOver){
            direction = 'left';
            snakeMove();
            clearInterval(interval);
            interval = setInterval(snakeMove,speed);
            interval;
        }
        else if(e.keyCode === 38 && direction !=='down' && direction !=='up' && !gameOver){
            direction = 'up';
            snakeMove();
            clearInterval(interval);
            interval = setInterval(snakeMove,speed);
            interval;
        }
        else if(e.keyCode === 39 && direction !=='left' && direction !=='right' && !gameOver){
            direction = 'right';
            snakeMove();
            clearInterval(interval);
            interval = setInterval(snakeMove,speed);
            interval;
        }
        else if(e.keyCode === 40 && direction !=='up' && direction !=='down' && !gameOver){
            direction = 'down';
            snakeMove();
            clearInterval(interval);
            interval = setInterval(snakeMove,speed);
            interval;

        }
    });

    restartButton.addEventListener('click', function () {
        restartGame();
    });
}

function randomGenerator() {
    return Math.floor(Math.random() * (boardScale));
}

function randomRGB() {
    let r = Math.floor(Math.random() * 256);
    let g = Math.floor(Math.random() * 256);

    return "rgb("+r+", "+g+", 0)";
}

